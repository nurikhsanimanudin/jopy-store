package com.growtopia.sarafarm;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SarafarmApplication {

	public static void main(String[] args) {
		SpringApplication.run(SarafarmApplication.class, args);
	}

}
