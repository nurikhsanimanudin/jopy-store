package com.growtopia.sarafarm.repositories;

import com.growtopia.sarafarm.models.Menu;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface MenuRepository extends JpaRepository<Menu, Long> {
    @Query(value = "SELECT t.menu_id,t.icon,t.name,t.url,t.parent_id FROM menu t WHERE t.parent_id is null", nativeQuery = true)
    List<Menu> findParentMenu();

//    @Query(value = "SELECT t.menu_id,t.icon , t.name , t.url , t.seq, t.parent_id FROM menu t \r\n" + //
//            "JOIN role_menu ro ON t.menu_id = ro.menu_id \r\n" + //
//            "JOIN role r ON ro.role_id = r.role_id \r\n" + //
//            "JOIN users_roles urole ON urole.role_id = r.role_id \r\n" + //
//            "JOIN user u ON u.user_id = urole.user_id \r\n" + //
//            "WHERE u.username = ?1 ", nativeQuery = true)
//    List<Menu> findMenuByUser(String userName);

    @Query(value = "SELECT m.menu_id, m.icon, m.name, m.role_id, m.url, m.parent_id FROM menu m " +
            " JOIN role r ON r.role_id = m.role_id " +
            " JOIN users u ON u.role_id = r.role_id " +
            " WHERE u.username = ?1 ", nativeQuery = true)
    List<Menu> findMenuByUser(String userName);

    @Query(value = "SELECT menu_id, icon, name, role_id, url, parent_id FROM menu WHERE role_id = 1", nativeQuery = true)
    List<Menu> listGuestMenu();
}
