package com.growtopia.sarafarm.config.interceptor;

import com.growtopia.sarafarm.models.Menu;
import com.growtopia.sarafarm.repositories.MenuRepository;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import java.util.*;

@Component
public class MenuInterceptor implements HandlerInterceptor {

    @Autowired
    MenuRepository menuRepository;

    private Map<String, List<MenuParentChild>> userMenus = new HashMap<>();

    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler,
                           @Nullable ModelAndView modelAndView) throws Exception {
        if (modelAndView != null) {
            List<MenuParentChild> menus = new ArrayList<>();
//            if (SecurityContextHolder.getContext().getAuthentication().isAuthenticated()) {
//                String username = SecurityContextHolder.getContext().getAuthentication().getName();
            menus.addAll(getMenuForGuest());


//            menus.addAll(getMenuForUser("ikhsan"));
//            }
//            menus.addAll(getMenuForGuest());
            modelAndView.addObject("menus", menus);
        }
    }

    @Cacheable("menus")
    public List<MenuParentChild> getMenuForGuest() {
        List<Menu> listMenu = menuRepository.listGuestMenu();
        List<MenuParentChild> menuTree = new ArrayList<>();

        // Memisahkan parent dan child menu
        List<Menu> parentMenus = new ArrayList<>();
        List<Menu> childMenus = new ArrayList<>();
        List<Menu> cucuMenus = new ArrayList<>();

        for (Menu menu : listMenu) {
            if (menu.getParent() == null) {
                parentMenus.add(menu);
            } else {
                childMenus.add(menu);
            }
        }

        for (Menu child : childMenus) {
            for (Menu menu : listMenu) {
                if (menu.getParent() != null) {
                    if (menu.getParent().getId() == child.getId()) {
                        cucuMenus.add(menu);
                    }
                }
            }
        }

        // Membangun hierarchy
        for (Menu parentMenu : parentMenus) {
            MenuParentChild parentChild = new MenuParentChild();
            parentChild.setId(parentMenu.getId());
            parentChild.setIcon(parentMenu.getIcon());
            parentChild.setUrl(parentMenu.getUrl());
            parentChild.setName(parentMenu.getName());
            parentChild.setId(parentMenu.getId());

            // Mencari dan menambahkan child menu yang sesuai
            for (Menu childMenu : childMenus) {
                if (childMenu.getParent() != null && childMenu.getParent().getId() == parentMenu.getId()) {
                    MenuParentChild child = new MenuParentChild();
                    child.setId(childMenu.getId());
                    child.setIcon(childMenu.getIcon());
                    child.setUrl(childMenu.getUrl());
                    child.setName(childMenu.getName());
                    child.setId(childMenu.getId());
                    parentChild.addChild(child);
                }

                for (Menu menu : cucuMenus) {
                    if (menu.getParent() != null && menu.getParent().getId() == childMenu.getId() && childMenu.getParent().getId() == parentMenu.getId()) {
                        MenuParentChild cucu = new MenuParentChild();
                        cucu.setId(menu.getId());
                        cucu.setIcon(menu.getIcon());
                        cucu.setUrl(menu.getUrl());
                        cucu.setName(menu.getName());
                        cucu.setId(menu.getId());
                        cucu.setParentId(menu.getParent().getId());
                        parentChild.addCucu(cucu);
                    }
                }
            }
            menuTree.add(parentChild);
        }

        return menuTree;
    }

    public List<MenuParentChild> getMenuForUser(String username) {
        // if (userMenus.containsKey(username)) {
        // System.out.println("get chace");
        // return userMenus.get(username);
        // } else {
        // System.out.println("simpan chace");
        // // Jika menu belum ada dalam cache, lakukan query dan simpan dalam cache
        // List<MenuParentChild> menus = performMenuQuery(username);
        // userMenus.put(username, menus);
        // return menus;
        // }
        List<MenuParentChild> menus = performMenuQuery(username);
        // userMenus.put(username, menus);
        return menus;
    }

    private List<MenuParentChild> performMenuQuery(String username) {

        List<Menu> listMenu = menuRepository.findMenuByUser(username);
        List<MenuParentChild> menuTree = new ArrayList<>();

        // Memisahkan parent dan child menu
        List<Menu> parentMenus = new ArrayList<>();
        List<Menu> childMenus = new ArrayList<>();

        for (Menu menu : listMenu) {
            if (menu.getParent() == null) {
                parentMenus.add(menu);
            } else {
                childMenus.add(menu);
            }
        }

        // Membangun hierarchy
        for (Menu parentMenu : parentMenus) {
            MenuParentChild parentChild = new MenuParentChild();
            parentChild.setId(parentMenu.getId());
            parentChild.setIcon(parentMenu.getIcon());
            parentChild.setUrl(parentMenu.getUrl());
            parentChild.setName(parentMenu.getName());

            // Mencari dan menambahkan child menu yang sesuai
            for (Menu childMenu : childMenus) {
                if (childMenu.getParent() != null && childMenu.getParent().getId() == parentMenu.getId()) {
                    MenuParentChild child = new MenuParentChild();
                    child.setId(childMenu.getId());
                    child.setIcon(childMenu.getIcon());
                    child.setUrl(childMenu.getUrl());
                    child.setName(childMenu.getName());
                    parentChild.addChild(child);
                }
            }

            menuTree.add(parentChild);
        }

        return menuTree;
    }
}

class MenuParentChild {
    private Long id;
    private String name;
    private String url;
    private String icon;
    private Long parentId;
    private List<MenuParentChild> children;

    private List<MenuParentChild> cucu;

    public void addCucu(MenuParentChild child) {
        if (cucu == null) {
            cucu = new ArrayList<>();
        }
        cucu.add(child);
    }

    public List<MenuParentChild> getCucu() {
        return cucu;
    }

    public void setCucu(List<MenuParentChild> cucu) {
        this.cucu = cucu;
    }

    public void addChild(MenuParentChild child) {
        if (children == null) {
            children = new ArrayList<>();
        }
        children.add(child);
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public Long getParentId() {
        return parentId;
    }

    public void setParentId(Long parentId) {
        this.parentId = parentId;
    }

    public List<MenuParentChild> getChildren() {
        return children;
    }

    public void setChildren(List<MenuParentChild> children) {
        this.children = children;
    }

}