package com.growtopia.sarafarm.models;

import jakarta.persistence.*;

import java.util.Collection;

@Entity
@Table(name = "role")
public class Role {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "role_id")
    private Long id;

    @Column(name="name")
    private String name;

//    @ManyToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
//    @JoinTable(name = "role_menu", joinColumns = @JoinColumn(name = "role_id", referencedColumnName = "role_id"),
//            inverseJoinColumns = @JoinColumn(name = "menu_id", referencedColumnName = "menu_id"))
//    private Collection<Menu> menus;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

//    public Collection<Menu> getMenus() {
//        return menus;
//    }
//
//    public void setMenus(Collection<Menu> menus) {
//        this.menus = menus;
//    }
}
