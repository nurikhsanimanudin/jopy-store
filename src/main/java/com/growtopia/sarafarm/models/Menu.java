package com.growtopia.sarafarm.models;

import jakarta.persistence.*;

@Entity
@Table(name = "menu")
public class Menu {
    @jakarta.persistence.Id
    @GeneratedValue(strategy = GenerationType.IDENTITY) // proses masukan data si id nya (identity = unique)
    @Column(name = "menu_id") // ini nama kolom yang akan dibuat di tabel
    private Long Id; // objek yg mengacu ke kolom di tabel

    private String url;
    private String name;
    @ManyToOne
    @JoinColumn(name = "parent_id")
    private Menu parent;

    private Long roleId;

    private String icon;

    public Long getRoleId() {
        return roleId;
    }

    public void setRoleId(Long roleId) {
        this.roleId = roleId;
    }

    public Long getId() {
        return Id;
    }

    public void setId(Long id) {
        Id = id;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Menu getParent() {
        return parent;
    }

    public void setParent(Menu parent) {
        this.parent = parent;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }
}
