package com.growtopia.sarafarm.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/discord")
public class DiscordController {

    @GetMapping("/")
    public String index() {
        return "guest/discord/index";
    }
}
